# Prosjekt 1 IMT2291 spring 2019

#### Group members: Simon Sandvoll, Eirik Vattøy, Zain Butt

In this project the group used PHP and Twig to created an web application for teachers and students. In this web application users can create an account and can be user-type of either teacher, students or admin. The first user-admin is created and is entered directly into database. If there is user type of teacher, he has to get approved by an admin user. Teachers can create/update/delete playlists and upload videos. Students can view, comment and rate videos, and subscribe to playlists created by a teacher.

## Code structure

Folder structure looks like this:

- imt2291-prosejkt1-2020
  - dbInit
    - init.sql
  - src
    - classes
      - Admin.php
      - DB.php
      - Playlist.php
      - Student.php
      - Teacher.php
      - TwigController.php
      - User.php
      - Validator.php
      - Video.php
    - css
    - twig_templates
    - img
    - accountPage.php
    - index.php
  - tests
  - gitignore
  - composer.json
  - docker-compose.yml
  - dockerfile.codeception
  - dockerfile.www
  - ReadMe.md

Twig_templates folder includes all the html files.
Classes folder includes all the classes with functionallity dependant on the type of user logged in. Index.php is responsible for routing of web application.
The group is using Docker to run it.
`docker-compose.yml` is where the server is set up and all ports are defined.

## How to run the web application?

### Windows & MAC

- Navigate to folder `imt2281-prosejkt1-2020`
- Install Docker
- Open terminal and write `docker-compose up -d`
- Go to `localhost:80/` in your browser to open up the platform
- To view the SQL tables go to `localhost:8080/`
  - With username `user` and password `test`

## Admin Access

To login wih pre created admin user, follow these steps:

- Login with this email `admin@admin.com`
- and with this password `admin`

## Validation

- User is validated when creating the account.
- User needs to use their email to login.
- Admin can approve or reject Teachers request.

## Explaintion

This solution can help teachers who record their lectures and later upload on any other platform for students to view. Sometimes there could be problem for teachers to find platform who support such a large video or students can have problems viewing it because it did not got upload correctly. if teacher have more than one course and have to upload videos for both courses, with this application it is easy to do that. Teachers and create a playlist with course-code name and upload video under its playlist. With this solution teachers have track of their lecture videos and at the same time students can view and rate and comments on lecture videos.

To test run these command in the terminal:

- `docker exec -it imt2291-prosjekt1-2020_test_1 bash` and press enter
- and then `codecept test` and enter.

The test will go through the core functionallity of the website; login, create user, create video, create playlist, validate teacher users, and give admin access.

### We are proud of:

- The way we integrate a twig class to render the templates, and the use of the method "indexOrError", to remove some of the repeating code.
- Of the clean way the index.php file looks, and the way that it creates an object of the usertype of the user logged in.

###### note: Two of our group members had problems with docker, and therefore one of us committed all the code.

## Sources

The group have used notes from lecture, stackoverflow and https://paulund.co.uk/php-delete-directory-and-files-in-directory
