<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/User.php';
require_once '../html/classes/Video.php';

class VideoCreateAcceptanceCest {
  protected $user, $video, $userData, $videoData;

  public function _before(AcceptanceTester $I) {
    $db = DB::getDBConnection();
    $username = md5(date('l jS \of F Y h:i:s A'));
    $fullname = md5(date('l jS h:i:s A \of F Y '));
    $email = $username.'@test.test';
    $password = 'MittHemmeligePassord';

    $this->userData['username'] = $username;
    $this->userData['fullname'] = $fullname;
    $this->userData['password'] = $password;
    $this->userData['password01'] = $password;
    $this->userData['password02'] = $password;
    $this->userData['email'] = $email;

    $this->videoData['name'] = 'video title';
    $this->videoData['description'] = 'video description';
    $this->videoData['thumbnail'] = '';
    $this->videoData['mime'] = '';

    $this->user = new User($db);
    $this->video = new Video($db);
  }

  public function OnFirstPage(AcceptanceTester $I) {
    $I->amOnPage('/');
    $I->see('Ikke logget inn');
  }

  public function testTeacherLogin(AcceptanceTester $I) {
    // Create teacher
    $this->userData['userType'] = 'teacher';
    $email = $this->user->addUser($this->userData)['email'];
    // Validate teacher 
    $this->user->updateUser($email);

    $I->amOnPage('/');
    $I->see('Ikke logget inn');

    $I->amOnPage('/');
    $I->fillField('email', $this->userData['email']);
    $I->fillField('password', $this->userData['password']);
    $I->click('Logg inn');

    $I->amOnPage('/');
    $I->click('Last opp en ny video');

    $I->see('Tittel');
    $I->fillField('name', $this->videoData['name']);
    $I->fillField('description', $this->videoData['description']);
    $I->attachFile('thumbnail','../acceptance/tmp.png');
    $I->click('Publiser video');

    $I->see('Video lagt til');
    $I->click('Vis dine videoer');
    $I->see('video title');

    $I->click('logg ut');
    $I->amOnPage('/');
    $I->see('Ikke logget inn');

    $this->user->deleteUser($email);
  }
}