<?php

require_once '../html/classes/DB.php';
require_once '../html/classes/Video.php';
require_once '../html/classes/User.php';

class VideoTest extends \Codeception\Test\Unit {
  /**
   * @var \UnitTester
   */
  protected $tester;
  protected $username, $videoData, $userData;
  protected $video;
  private $password = 'mySecretPassword';
  private $db;

  protected function _before() {
    $db = DB::getDBConnection();
    $this->username = md5(date('l jS \of F Y h:i:s A'));
    $this->fullname = md5(date('l jS \of F Y h:i:s A'));
    $this->email = $this->username.'@test.test';

    $this->userData['username'] = $this->username;
    $this->userData['fullname'] = $this->fullname;
    $this->userData['password'] = $this->password;
    $this->userData['password01'] = $this->password;
    $this->userData['password02'] = $this->password;
    $this->userData['email'] = $this->username.'@test.test';
    
    $this->videoData['name'] = 'video title';
    $this->videoData['description'] = 'video description';
    $this->videoData['thumbnail'] = '';
    $this->videoData['mime'] = '';

    $this->user = new User($db);
    $this->video = new Video($db);
  }

  protected function _after() {
    
  }

  public function testVideoCreationCommentingRating() {
    // Create teacher
    $this->userData['userType'] = 'teacher';
    $teacherCreate = $this->user->addUser($this->userData);   
    $this->assertEquals('OK', $teacherCreate['status'], 'Failed to create new teacher.');
    $this->assertTrue(isset($teacherCreate['email']), 'Email is not set');

    // Create video
    $this->videoData['username'] = $this->username;
    $videoData = $this->video->createVideo($this->videoData);
    $this->assertEquals('OK', $videoData['status'], 'Failed to create new video.');
    $this->assertTrue(isset($videoData['id']), 'Id of video is not set');

    // Change into new user, student type.
    $this->username = md5(date("F j, Y, g:i a"));
    $this->fullname = md5(date("F j, Y, g:i a"));
    $this->email = $this->username.'@test.test';
    $this->userData['username'] = $this->username;
    $this->userData['fullname'] = $this->fullname;
    $this->userData['email'] = $this->username.'@test.test';
    $this->userData['userType'] = 'student';

    // Create student
    $studentCreate = $this->user->addUser($this->userData);
    $this->assertEquals('OK', $studentCreate['status'], 'Failed to create new student.');
    $this->assertTrue(isset($studentCreate['email']), 'Email is not set');

    //Add comment
    $commentText = 'Her er en kommentar!';
    $commentArray = array('username' => $this->username, 
      'videoId' => $videoData['id'], 'commentText' => $commentText);
    $commentCreate = $this->video->postComment($commentArray);
    $this->assertEquals('OK', $commentCreate['status'], 'Failed to create comment');
    $this->assertTrue(isset($commentCreate['id']), 'Id of comment is not set');

    // Add rating
    $rate = 5;
    $ratingArray = array('username' => $this->username, 
      'videoId' => $videoData['id'], 'rate' => $rate);
    $rateCreate = $this->video->rateVideo($ratingArray);
    $this->assertEquals('OK', $rateCreate['status'], 'Failed to add rating');

    // Delete video
    $deleted = $this->video->deleteVideo($videoData['id'], $this->videoData['username']);
    $this->assertEquals('OK', $deleted['status'], 'Video could not be deleted.');

    // Delete teacher
    $deleteTeacher = $this->user->deleteUser($teacherCreate['email']);
    $this->assertEquals('OK', $deleteTeacher['status'], 'Teacher could not be deleted.');

    // Delete student
    $deleteStudent = $this->user->deleteUser($studentCreate['email']);
    $this->assertEquals('OK', $deleteStudent['status'], 'Student could not be deleted.');
  }
}
