<?php

require_once 'Validator.php';

class Student {

	private $twig = null;
	private $video = null;
	private $playlist = null;

	public function __construct($twig, $video, $playlist) {
		$this->twig = $twig;
		$this->video = $video;
		$this->playlist = $playlist;
		$this->twig->setGlobal('user', $_SESSION['userData']);

		if (isset($_GET['showSubscriptions'])) {
			$tmp = $this->playlist->getPlaylists($_SESSION['userData']['username'], null, true);

			if ($tmp['status'] === 'OK') {
				$this->twig->render('index.html', array('view' => 'showSubscriptions', 
					'playlists' => $tmp['data']));
			} else {
				$this->twig->render('index.html', array('view' => 'showSubscriptions', 
					'message' => 'Du har ingen abonnementer!'));
			}

		} else if (isset($_GET['openVideo'])) {
			$tmp = $this->video->getVideos(null, $_GET['openVideo'], null);
			if ($tmp['status'] === 'OK') {
				$updateView = $this->video->updateVideo(array('idToUpdate' => $_GET['openVideo']), true);
				if ($updateView['status'] === 'FAIL') {
					$this->twig->render('error.html', $updateView);
				}
				$this->twig->render('index.html', array('view' => 'openVideo', 
					'video' => $tmp['data'], 'comments' => $tmp['comments']));
			} else {
				$this->twig->render('error.html', $tmp);
			}

		} else if (isset($_GET['search'])) {
			$this->search($_GET['search']);

		} else if ($_GET['openPlaylist']) { 
			$tmp = $this->playlist->getPlaylists(null, $_GET['openPlaylist']);
			$this->twig->indexOrError($tmp, array('view' => 'openPlaylist', 
				'playlist' => $tmp['playlist'], 'videos' => $tmp['videos']));
		
		} else if ($_GET['unsubscribeToPlaylist']) { 
			$tmp = $this->playlist->unsubscribeToPlaylist($_GET['unsubscribeToPlaylist'], 
				$_SESSION['userData']['username']);
			$this->twig->indexOrError($tmp, array());

		} else if (isset($_POST['commentText'])) {
			$this->commentOnVideo($_POST, $_SESSION['userData']['username']);
			
		} else if (isset($_POST['rate'])) {
			$this->rateVideo($_POST, $_SESSION['userData']['username']);
			
		} else if (isset($_POST['subscribe'])) {
			$tmp = $this->playlist->subscribeToPlaylist($_POST['playlistId'], 
				$_SESSION['userData']['username']);
			$this->twig->indexOrError($tmp, array('title' => 'Abonnering vellykket'));

		} else {
			$getNewVideos = $this->video->getVideos(null, null, '5');
			$getPlaylists = $this->playlist->getPlaylists(
				$_SESSION['userData']['username'], null, true);
			$displayArray = array();
			if ($getPlaylists['status'] === 'OK') {
				$displayArray['playlists'] = $getPlaylists['data'];
			} else {
				$displayArray['playlistMessage'] = "Du har ingen abonnementer!";
			}
			if ($getNewVideos['status'] === 'OK') {
				$displayArray['videos'] = $getNewVideos['data'];
			} else {
				$displayArray['videoMessage'] = "Det finnes ingen videoer her!";
			}
			$this->twig->render('index.html', $displayArray);
		}
	}

	public function search($searchWord) {
		if (strlen($searchWord) > 2) {
			$vTmp = $this->video->search($searchWord);
			$displayArray = array();
			$displayArray['view'] = 'search';
			if ($vTmp['status'] === 'OK') {
				$displayArray['videos'] = $vTmp['data'];
				$displayArray['videoMessage'] = 'Fant ' . count($vTmp['data']) . ' videoer';
			} else {
				$displayArray['videoMessage'] = 'Fant ingen videoer med dette søkeordet!';
			}

			$pTmp = $this->playlist->search($searchWord);

			if ($pTmp['status'] === 'OK') {
				$displayArray['playlists'] = $pTmp['data'];
				$displayArray['playlistMessage'] = 'Fant ' . count($pTmp['data']) . ' spillelister';
			} else {
				$displayArray['playlistMessage'] = 'Fant ingen spillelister med dette søkeordet!';
			}

			$this->twig->render('index.html', $displayArray);
		} else {
			$this->twig->render('error.html', array(
				'errorMessage' => 'Søkeordet må være større enn 2 bokstaver'));
		}
	}

	private function commentOnVideo($formData, $username) {
		$dataToSend = array('username' => $username, 
			'videoId' => $formData['videoId'], 
			'commentText' => $formData['commentText']);
		$valid = Validator::validateCommentPost($dataToSend);
		if ($valid['status'] === 'OK') {
			$tmp = $this->video->postComment($dataToSend);
			$this->twig->indexOrError($tmp, array('title' => 'Kommentar lagt til'));
		} else {
			$this->twig->render('error.html', $valid);
		}
	}

	private function rateVideo($formData, $username) {
		$dataToSend = array('username' => $username, 
				'videoId' => $formData['videoId'], 'rate' => $formData['rate']);
			$valid = Validator::validateRate($dataToSend);
			if ($valid['status'] === 'OK') {
				$tmp = $this->video->rateVideo($dataToSend);
				$this->twig->indexOrError($tmp, array('title' => 'Vurderingen lagt til'));
			} else {
				$this->twig->render('error.html', $valid);
			}
	}
}

?>