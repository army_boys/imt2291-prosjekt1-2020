<?php
/**
 * This class is for adding, selecting and editing users in
 * the database. Gets function "validateLoginData" and 
 * "validateCreateUserData from the Validator class file
 */


class User {
  private $email;
  private $userData = [];
  private $db;

  /**
   * The construct function handles all the GET and POST requests
   * related to the user. 
  */
  public function __construct($db) {
    $this->db = $db;
/*     if (isset($_POST['loginUser'])) {
      $valid = Validator::validateLoginData($_POST['email'], $_POST['password']);
      if ($valid['status'] === 'OK') {
        $this->loginUser($_POST['email'], $_POST['password']);
      }
    } else if (isset($_POST['logout'])) {
      $this->logout();

    } else if (isset($_POST['createUser'])) {
      $valid = Validator::validateCreateUserData($_POST);
      if ($valid['status'] === 'OK') {
        $this->addUser($_POST);
      }
    } */
  }

  public function __destruct() {
    if ($this->db!=null) {
      unset ($this->db);
    }
  }
  
  /**
   * Checks if the user is logged in
   * @return bool true if the email value has a value.
  */
  public function loggedIn() {
    return isset($_SESSION['userData']['email']);
  }

  /** 
   * @param array $data of all user info, being stored in the session
   * everything except the password. 
   * @return array $tmpData cleaned array ready for use
  */
  public function setUserData($data) {
    $tmpData = array('username' => $data['username'], 
      'fullname' => $data['fullname'], 
      'email' => $data['email'], 
      'userType' => $data['userType'], 
      'validated' => $data['validated'],
      'loggedIn' => 'yes');
    $_SESSION['userData'] = $tmpData;
    return $tmpData;
  }

/**
 * Adds a user to the database.
 * @param array with 'username', 'fullname', 'email', 'password' and 'userType'.
 * @return an array with only element 'status'=='OK' on success.
 *        'status'=='FAIL' on error, the error info can be found
 *        in 'errorInfo'.
 */
  public function addUser ($data) {
 
    $sql = 'INSERT INTO Users 
        (username, fullname, email, password, userType, validated) 
        VALUES (?, ?, ?, ?, ?, ?)';
    $sth = $this->db->prepare($sql);

    // if user tries to create teacher or admin account set validated to 0 
    // because the admin needs to validate this type of user first
    $data['validated'] = ($data['userType'] === 'teacher' || $data['userType'] === 'admin' ? 0 : 1);

    $tmp = [];
    $hashedPass = password_hash($data['password01'], PASSWORD_DEFAULT);
    
    $sqlData = array($data['username'], $data['fullname'],
      $data['email'], $hashedPass, 
      $data['userType'], $data['validated']);
    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['email'] = $data['email'];
      $this->userData = $this->setUserData($data);
      $this->email = $data['email'];
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke lagd bruker!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  
  /**
   * Login a user.
   * @param String $email the email of the user trying to log in
   * @param String $password the password of the user trying to log in
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
   */
  public function loginUser($email, $password) {
    $sql = 'SELECT username, fullname, 
      email, password, userType, validated 
      FROM Users WHERE email=?';

    $sth = $this->db->prepare ($sql);
    $sth->execute(array($email));

    $row = $sth->fetch(PDO::FETCH_ASSOC);

    if ($row) {
      if (password_verify($password, $row['password'])) {
        $this->userData = $this->setUserData($row);
        $this->email = $row['email'];
        return array('status' => 'OK', 'email' => $row['email']);
      } else {
        $data['status'] = 'FAIL';
        $data['errorMessage'] = 'Feil brukernavn eller passord';
        return $data;
      }
    } else {
      $data['status'] = 'FAIL';
      $data['errorMessage'] = 'Fant ikke brukeren, har du skrevet riktig brukernavn?';
      return $data;
    }
  }

  public function logout() {
    unset($this->email);
    unset($this->userData);
    session_unset();
  }

 /**
   * Updates a user in the database.
   * @param string $email of the user to be updated.
   * @param bool $validate checks if the user is being valudated or given admin access.
   * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure, 
   * together with a message.
   */
  public function updateUser($email, $validate = true) {
    if ($validate === true) {
    $sql = "UPDATE Users 
           SET validated=1 
           WHERE email=?";
    } else {
      $sql = "UPDATE Users 
      SET userType='admin' 
      WHERE email=?";
    }

    $sth = $this->db->prepare ($sql);
    $sth->execute (array($email));
    $tmp = [];
    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Failed to update user';
    }
    return $tmp;
  }

     /**
   * Selects users in the database either where validated = 0 or where the usertype
   * is not 'admin';
   * @param string $type checks if all users are being recived or not. 'all' or 'valid'.
   * @param string $email of the user making the get request, meaning that they don't 
   * fetch data about themselves. 
   * @return array with the element 'status' set to 'OK' on success, 'OK' on failure.
   * on success the element 'data contains the same data about the users. On failure
   * the array is empty, but no error message will show.
   */
  public function getUsers($type = '', $email = null){
    if ($type === 'all') {
      $sql = "SELECT * FROM Users WHERE email <> ? AND userType <> 'admin'";
      $sth = $this->db->prepare($sql);
      $sth->execute(array($email));
    } else {
      $sql = "SELECT * FROM Users WHERE validated=0";
      $sth = $this->db->prepare($sql);
      $sth->execute();
    }
    $dataFromDB = $sth->fetchAll();
    $tmp = [];
    if($dataFromDB){
      $tmp['status'] = 'OK';
      $tmp['data'] = $dataFromDB;
    } else {
      $tmp['status'] = 'OK';
      $tmp['data'] = array();
      $tmp['errorMessage'] = 'Failed to fetch user';
    }
    return $tmp;
  }

  /**
   * Deletes the user with the given email from the database.
   * @param  String $email the email of the user to be deleted
   * @return Array the elements status=OK if success, else status=FAIL
   */
  public function deleteUser($email) {
    $sql = 'DELETE FROM Users WHERE email=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($email));
    if ($sth->rowCount() == 1) {
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL');
    }
  }
}
