<?php

class Admin {

	private $user = null;
	private $twig = null;

	public function __construct($user, $twig) {
		$this->user = $user;
		$this->twig = $twig;
		$this->twig->setGlobal('user', $_SESSION['userData']);

		if (isset($_GET['viewUsers'])) {
			$this->viewUsers($_GET['viewUsers']);
		}
		
		else if (isset($_GET['changeUser'])) {
			$this->changeUser($_GET['changeUser']);
		}
		
		else if (isset($_GET['giveAccess'])) {
			$this->giveAccess($_GET['giveAccess']);
		
		} else {
			$this->twig->render('index.html', array());
		}
	}

	public function viewUsers($type) {
		$usersFromDB = $type == 'validate' ? 
		$this->user->getUsers() : 
		$this->user->getUsers('all', $_SESSION['userData']['email']);

		$this->twig->indexOrError($usersFromDB, array(
			'showUsers' => $_GET['viewUsers'], 'users' => $usersFromDB['data']));
	}

	public function changeUser($idOfUserToChange) {
		$userToChange = $this->user->updateUser($idOfUserToChange);
		$this->twig->indexOrError($userToChange, array(
			'showUsers' => 'validate', 'users'=> array(), 'status' => 'OK'));
	}
	
	public function giveAccess($idOfUserToGiveAccess) {
		$userToChange = $this->user->updateUser($idOfUserToGiveAccess, false);
		$this->twig->indexOrError($userToChange, array(
			'showUsers' => 'all', 'users'=> array(), 'status' => 'OK'));
	}
}

?>