<?php

class Validator {

/**
 * Gets $_POST data to be validated against some requirements.
 * @param string $email of the user trying to login.
 * @param string $password of the user trying to login.
 * @return array with status 'OK' on success and 'FAIL' on fail.
 * On Fail an array of errorInfo is attached to the array. 
 */
  public function validateLoginData($email = null, $password = null){
    $tmp['status'] = 'OK';
    $tmp['errorInfo'] = [];
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        array_push(
            $tmp['errorInfo'], 
            'Eposten må være en ekte epost');
    }
    if (!preg_match("/^[a-zA-Z]([\w -]*[a-zA-Z])?$/", $password)) {
        array_push(
            $tmp['errorInfo'], 
            'Passordet inneholder kun bokstaver, tall, bindestrek, understrek og mellomrom');
    }
    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    return $tmp; 
  }
  /**
  * Checks if form data is valid.
  * @param data an assosiative array with username, fullName, 
  *         email, password, userType of the user to be inserted into the database.
  * @return array with the element 'status' set to 'OK' on success, 'FAIL' on failure.
  *        on FAIL the errorInfo array will be displayed under the form informing the user
  *        about the errors in the form. If the data is OK the user will be created.
  */
  public function validateCreateUserData($formData){
    $tmp['status'] = 'OK';
    $tmp['errorInfo'] = [];
    if (strlen($formData['username']) < 3 && ctype_alnum($formData['username'])) {
        array_push(
            $tmp['errorInfo'], 
            'Brukernavnet må være lengere enn 3 bokstaver, også bare bokstaver og tall');
    }
    if (strlen($formData['fullName']) < 4 
        && !preg_match("/^[a-zA-Z ]*$/",$formData['fullName'])) {
        array_push(
            $tmp['errorInfo'], 
            'Navnet må være lengere enn 4 bokstaver, med bare bokstaver og mellomrom');
    }
    if (!filter_var($formData['email'], FILTER_VALIDATE_EMAIL)) {
        array_push(
            $tmp['errorInfo'], 
            'Eposten må være en ekte epost');
    }
    if (!preg_match("/^[a-zA-Z]([\w -]*[a-zA-Z])?$/", $formData['password01'])) {
        array_push(
            $tmp['errorInfo'], 
            'Passordene må kun inneholde bokstaver, tall, bindestrek, understrek og mellomrom');
    }
    if ($formData['password01'] !== $formData['password02']) {
        array_push(
            $tmp['errorInfo'], 
            'De to passordene må være like');
    }
    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    return $tmp; 
  }

  /**
   * Gets $_POST and $_FILES data to be validated against some requirements.
   * @param array $formData, with title, description, course, topic.
   * @param array $files, with the thumbnail being inserted into the database.
   * @return array with status 'OK' on success and 'FAIL' on fail.
   * On Fail an array of errorInfo is attached to the array. 
  */
  public function validatePlaylistSubmit($formData, $file) {

    $tmp['status'] = 'OK';
    $tmp['errorInfo'] = [];

    $target_file = basename($file["thumbnail"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if (strlen($file["thumbnail"]['name']) < 1 ) {
      array_push($tmp['errorInfo'], 'Du må laste opp en thumbnail!');
    } else {
      $check = getimagesize($file["thumbnail"]["tmp_name"]);
      if($check === false) {
        array_push(
          $tmp['errorInfo'], 
          'Filen er ikke et bilde');
      }
    }

    if ($file["thumbnail"]["size"] > 500000) {
      array_push($tmp['errorInfo'], 
        'Filen er for stor, må være mindre enn 0.5mb');
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
      array_push($tmp['errorInfo'], 
        'Bare JPG, PNG, og JPEG formater er tillatt');
    }

    if (strlen($formData['title']) < 3 && ctype_alnum($formData['title'])) {
      array_push(
          $tmp['errorInfo'], 
          'Tittelen må være lengere enn 3 bokstaver, også bare bokstaver og tall');
    }

    if (strlen($formData['description']) < 5) {
      array_push(
        $tmp['errorInfo'], 
        'Beskrivelsen må være lengere enn 5 bokstaver');
    }

    if (strlen($formData['topic']) < 3 && ctype_alnum($formData['topic'])) {
      array_push(
        $tmp['errorInfo'], 
        'Emnet må være lengere enn 3 bokstaver, også bare bokstaver og tall');
    }

    if (strlen($formData['course']) < 3 && ctype_alnum($formData['course'])) {
      array_push(
        $tmp['errorInfo'], 
        'Faget må være lengere enn 3 bokstaver, også bare bokstaver og tall');
    }


    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    $tmp['view'] = ($tmp['status'] === 'FAIL' ? 'error' : null);

    return $tmp;
  }

    /**
   * Gets $_POST and $_FILES data to be validated against some requirements.
   * @param array $formData, with name and description
   * @param array $files, with the thumbnail being inserted into the database.
   * @return array with status 'OK' on success and 'FAIL' on fail.
   * On Fail an array of errorInfo is attached to the array. 
  */
  function validateVideoSubmit($formData, $file) {

    $tmp['status'] = 'OK';
    $tmp['errorInfo'] = [];

    $target_file = basename($file["thumbnail"]["name"]);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

    if (strlen($file["thumbnail"]['name']) < 1 ) {
      array_push($tmp['errorInfo'], 'Du må laste opp en thumbnail!');
    } else {
      $check = getimagesize($file["thumbnail"]["tmp_name"]);
      if($check === false) {
        array_push(
          $tmp['errorInfo'], 
          'Filen er ikke et bilde');
      }
    }

    if ($file["thumbnail"]["size"] > 500000) {
      array_push($tmp['errorInfo'], 
        'Filen er for stor, må være mindre enn 0.5mb');
    }

    if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg") {
      array_push($tmp['errorInfo'], 
        'Bare JPG, PNG, og JPEG formater er tillatt');
    }

    if (strlen($formData['name']) < 3 && ctype_alnum($formData['name'])) {
      array_push(
          $tmp['errorInfo'], 
          'Tittelen må være lengere enn 3 bokstaver, også bare bokstaver og tall');
    }

    if (strlen($formData['description']) < 5) {
      array_push(
        $tmp['errorInfo'], 
        'Beskrivelsen må være lengere enn 5 bokstaver');
    }

    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    $tmp['view'] = ($tmp['status'] === 'FAIL' ? 'error' : null);
    return $tmp;
  }

  /**
  * Gets $_POST data to be validated against some requirements. Before insertion
  * of the comment.
  * @param array $data, with the text of the comment
  * @return array with status 'OK' on success and 'FAIL' on fail.
  * On Fail an array of errorInfo is attached to the array. 
  */
  function validateCommentPost($data) {
    $tmp['errorInfo'] = [];
    if (strlen($data['commentText']) < 4) {
      array_push(
        $tmp['errorInfo'], 
        'Kommentaren må være lengere enn 4 bokstaver');
    }
    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    return $tmp;
  }

  /**
  * Gets $_POST data to be validated against some requirements. Before insertion
  * of the rating.
  * @param array $data, with the number of the rating.
  * @return array with status 'OK' on success and 'FAIL' on fail.
  * On Fail an array of errorInfo is attached to the array. 
  */
  function validateRate($data) {
    $tmp['status'] = 'OK';
    $tmp['errorInfo'] = [];

    if ($data['rate'] < 1) {
      array_push(
        $tmp['errorInfo'], 
        'Rate må være høyere enn eller er lik 1 bokstaver');
    }
    $tmp['status'] = (count($tmp['errorInfo']) === 0 ? 'OK' : 'FAIL');
    return $tmp;
  }
}
?>