<?php

/**
 * This class is for adding, editing, deleting
 * and searching for videos in the lecturevideos database.
*/
class Video {
  private $db = null;
  public $width = 150;
  public $height = 150;

  /**
   * Connect to the database when object is created.
  */
  public function __construct($db) {
    $this->db = $db;
  }

  /** 
   * Function that prepares the data for insertion, meaning encoding the thumbnail,
   * and all other data about the video being inserted/updated. 
   * @param array $formData is all the data from the update/insert post request.
   * @param array $file is the file information from the update/insert post request. 
   * @param string $username. When updating the table we don't need the username,
   * but when inserting we do. 
   * @return array with the data needed for insertion/deletion. "name", "description",
   * "thumbnail" and "username"/"idToUpdate".
  */
  public function prepareDataForInsertion($formData, $file, $username) {
    $thumbnail = file_get_contents(
        $file['thumbnail']['tmp_name']);
    $scaledThumbnail = $this->scale(imagecreatefromstring(
      $thumbnail), $this->width, $this->height);
    unset($thumbnail);
    $mime = $file['thumbnail']['type'];
    $fileExtension = "png";
    if ($mime === "image/png") {
      $fileExtension = "png";
    } else if ($mime === "image/jpeg") {
      $fileExtension = "jpeg";
    } else if ($mime === "image/jpg") {
      $fileExtension = "jpg";
    }
    $video = $file['video']['name'];
    $filePath = $file['thumbnail']['tmp_name'];
    $data = ["name" => $formData['name'], "description" => $formData['description'], 
      "thumbnail" => $scaledThumbnail, "mime" => $mime, 
      'filePath' => $filePath, "fileExtension" => $fileExtension,
      'video' => $video, 'videoPath' => $file['video']['tmp_name']];
  
    $data['username'] = $username;
    if (isset($formData['id'])) {
      $data['idToUpdate'] = $formData['id'];
    }
    return $data;
  }

  /**
   * Adds a video to the database.
   * @param array with 'username', 'name', 'description' and 'tumbnail'.
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
  */
  public function createVideo($data) {

    $videoId = uniqid();

    $sql = "INSERT INTO Video 
      (id, userId, name, description, thumbnail, mime) 
      VALUES (?, ?, ?, ?, ?, ?)";
    
    $sth = $this->db->prepare($sql);

    $sqlData = array($videoId, $data['username'],
      $data['name'], $data['description'], $data['thumbnail'], 
      $data['mime']);

    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      if ($data['thumbnail'] !== '') {
        $username = $data['username'];
        $filePath = $data['filePath'];
        $fileExtension = $data['fileExtension'];

        if(!is_dir("uploadedFiles/$username")) {
          @mkdir("uploadedFiles/$username");
        }
        if(!is_dir("uploadedFiles/$username/$videoId")) {
          @mkdir("uploadedFiles/$username/$videoId");
        }
        $thumbnailDestination = "uploadedFiles/$username/$videoId/thumbnail.$fileExtension";
        if (!file_exists($thumbnailDestination)) {
          if(@move_uploaded_file($filePath, $thumbnailDestination)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Could not upload files';
          }
        } else {
          $this->deleteDirectoryOrFile($thumbnailDestination);
          if(@move_uploaded_file($filePath, $thumbnailDestination)) {
            $tmp['status'] = 'OK';
          } else {
            $tmp['status'] = 'FAIL';
            $tmp['errorMessage'] = 'Could not upload file';
          }
        }
      }
      $tmp['id'] = $videoId;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke lagd video!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Updates a video in the database.
   * @param array with 'idToUpdate', 'username', 'name', 
   *  'description' and 'tumbnail'.
   * @param bool $viewIncrease checks if the video that is being updated
   * is just an increase of views on that video or the data surrounding the 
   * video: name, descrioption, id.
   * @return an array with only element 'status'=='OK' on success.
   *        'status'=='FAIL' on error, the error info can be found
   *        in 'errorInfo'.
  */
  public function updateVideo($data, $viewIncrease = false) {
    $idToUpdate = $data['idToUpdate'];
    if ($viewIncrease === true) {
      $sql = "UPDATE Video SET views = views + 1
      WHERE id=?";
      $sth = $this->db->prepare($sql);
      $sqlData = array($idToUpdate);
    } else {
      $sql = "UPDATE Video SET
      name=?, description=?, thumbnail=?, mime=?
      WHERE id=?";
      $sth = $this->db->prepare($sql);
      $sqlData = array($data['name'], $data['description'],
        $data['thumbnail'], $data['mime'], $idToUpdate);
    }

    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      if ($viewIncrease === false) {
        $tmp['status'] = 'OK';
        if ($data['thumbnail'] != '') {
          $username = $data['username'];
          $filePath = $data['filePath'];
          $fileExtension = $data['fileExtension'];
          if(!is_dir("uploadedFiles/$username/$idToUpdate")) {
            @mkdir("uploadedFiles/$username/$idToUpdate");
          }
          if (!file_exists("uploadedFiles/$username/$idToUpdate/thumbnail.$fileExtension")) {
            if(@move_uploaded_file($filePath, "uploadedFiles/$username/$idToUpdate/thumbnail.$fileExtension")) {
              $tmp['status'] = 'OK';
            } else {
              $tmp['status'] = 'FAIL';
              $tmp['errorMessage'] = 'Could not upload file';
            }
          } else {
            $this->deleteDirectoryOrFile("uploadedFiles/$username/$idToUpdate/thumbnail.$fileExtension");
            if(@move_uploaded_file($filePath, "uploadedFiles/$username/$idToUpdate/thumbnail.$fileExtension")) {
              $tmp['status'] = 'OK';
            } else {
              $tmp['status'] = 'FAIL';
              $tmp['errorMessage'] = 'Could not upload file';
            }
          }
        }
      } else {
        $tmp['status'] = 'OK';
      }
    } else {
      $tmp['status'] = 'OK';
      $tmp['errorMessage'] = 'Ingenting endret!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Deletes the video with the given ID from the database.
   *
   * @param  String $id the id of the video to be deleted
   * @return Array with the elements status=OK if success, else status=FAIL
  */
  public function deleteVideo($id, $username) {
    $sql = 'DELETE FROM Video WHERE id=?';
    $sth = $this->db->prepare($sql);
    $sth->execute(array($id));
    if ($sth->rowCount() == 1) {
      $this->deleteDirectoryOrFile("uploadedFiles/$username/$id");
      return array('status'=>'OK');
    } else {
      return array('status'=>'FAIL');
    }
  }

  /* Taken from https://paulund.co.uk/php-delete-directory-and-files-in-directory */
  public function deleteDirectoryOrFile($target) {
    if(is_dir($target)){
      $files = glob( $target . '*', GLOB_MARK ); //GLOB_MARK adds a slash to directories returned
      foreach( $files as $file ){
        $this->deleteDirectoryOrFile($file);      
      }
      @rmdir( $target );
    } else if (is_file($target)) {
      @unlink( $target );  
    }
  }

  /**
   * Gets a video to the database.
   * @param string $userId of the user that created the video
   * @param string 'id' of the video to be selected.
   * @param int $limit, limiting the number of videos being displayed.
   *  used when showing the newly posted videos. 
   * @return array with the video information; 'username', 'title', 
   *              'description' and 'thumbnail'.
  */
  public function getVideos($userId = null, $videoId = null, $limit = null) {
    if ($videoId === null && $userId !== null && $limit === null) {
      $sql = "SELECT v.id, v.userId, v.name, v.description, v.views, v.time, v.mime,
        avg(l.vote) as 'rating' 
        FROM Video v LEFT JOIN UserLike l 
        ON v.id = l.videoId WHERE v.userId=? GROUP BY v.id;";
      $sth = $this->db->prepare($sql);
      $sqlData = array($userId);
      $sth->execute($sqlData);
      $row = $sth->fetchAll();
    } else if ($videoId !== null && $userId === null && $limit === null) {
      $sql = "SELECT id, userId, name, description, views, time, mime,
        (SELECT avg(vote) FROM UserLike WHERE videoId=:videoId) as 'rating' 
        FROM Video WHERE id=:videoId";
      $sth = $this->db->prepare($sql);
      $sth->bindParam(':videoId', $videoId, PDO::PARAM_STR);
      $sth->execute();
      $row = $sth->fetch(PDO::FETCH_ASSOC);
    } else if ($videoId === null && $userId === null && $limit !== null) {
      $sql = "SELECT v.id, v.userId, v.name, v.description, v.views, v.time, v.mime,
        avg(l.vote) as 'rating' 
        FROM Video v LEFT JOIN UserLike l 
        ON v.id = l.videoId GROUP BY v.id ORDER BY time DESC LIMIT :limit;";
      $sth = $this->db->prepare($sql);
      $sth->bindParam(':limit', $limit, PDO::PARAM_INT);
      $sth->execute();
      $row = $sth->fetchAll();
    }
    $tmp = [];
    if ($row) {
      if ($videoId === null || $limit !== null) {
        for ($x = 0; $x < count($row); $x++) {
          $fileExtension = "png";
          if ($row[$x]['mime'] === "image/png") {
            $row[$x]['fileExtension'] = "png";
          } else if ($row[$x]['mime'] === "image/jpeg") {
            $row[$x]['fileExtension'] = "jpeg";
          } else if ($row[$x]['mime'] === "image/jpg") {
            $row[$x]['fileExtension'] = "jpg";
          }
        }
        $tmp['status'] = 'OK';
        $tmp['data'] = $row;
      } else {
        $fileExtension = "png";
        if ($row['mime'] === "image/png") {
          $fileExtension = "png";
        } else if ($row['mime'] === "image/jpeg") {
          $fileExtension = "jpeg";
        } else if ($row['mime'] === "image/jpg") {
          $fileExtension = "jpg";
        }
        $row['fileExtension'] = $fileExtension; 
        $comments = $this->getComments($row['id']);
        if ($comments['status'] === 'OK') {
          $tmp['status'] = 'OK';
          $tmp['data'] = $row;
          $tmp['comments'] = $comments['data'];
        }
      }
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fant ikke videoen';
    }
    return $tmp;
  }

  /**
   * Gets a video to the database.
   * @param string $userId of the user that created the video
   * @param string 'id' of the video to be selected.
   * @param int $limit, limiting the number of videos being displayed.
   *  used when showing the newly posted videos. 
   * @return array with the video information; 'username', 'title', 
   *              'description' and 'thumbnail'.
  */
  public function getOtherVideos($playlistId) {
    $sql = "SELECT v.id, v.userId, v.name, v.description, v.views, v.time, v.mime,
      avg(l.vote) as 'rating' 
      FROM Video v 
      LEFT JOIN UserLike l ON v.id = l.videoId 
       WHERE v.id NOT IN 
        (SELECT videoId FROM VideosInPlaylist 
        WHERE playlistId=?) GROUP BY v.id;";
    $sth = $this->db->prepare($sql);
    $sqlData = array($playlistId);
    $sth->execute($sqlData);
    $row = $sth->fetchAll();
    $tmp = [];
    if ($row) {
      for ($x = 0; $x < count($row); $x++) {
        $fileExtension = "png";
        if ($row[$x]['mime'] === "image/png") {
          $row[$x]['fileExtension'] = "png";
        } else if ($row[$x]['mime'] === "image/jpeg") {
          $row[$x]['fileExtension'] = "jpeg";
        } else if ($row[$x]['mime'] === "image/jpg") {
          $row[$x]['fileExtension'] = "jpg";
        }
      }
      $tmp['status'] = 'OK';
      $tmp['data'] = $row;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fant ikke videoen';
    }
    return $tmp;
  }
  
  /**
   * Retrieves the comments on the video
   * @param string $videoId. id of the video
   * @param array the comments from the video; userId, comment, and 
   * what time the comment was posted. Status is OK, even though
   * there are no comments, since not all videos have comments. 
  */
  public function getComments($videoId) {
    $sql = "SELECT userId, comment, time FROM Comment
      WHERE videoId=?";
    $sth = $this->db->prepare($sql);
    $sqlData = array($videoId);
    $sth->execute($sqlData);
    $row = $sth->fetchAll();
    $tmp = [];
    if ($row) {
      $tmp['status'] = 'OK';
      $tmp['data'] = $row;
    } else {
      $tmp['status'] = 'OK';
      $tmp['data'] = array();
    }
    return $tmp;
  }

  /**
   * Adds a comment to the Comment table
   * @param array of userId, videoId, and comment.
   * @return array with status 'OK' on success and 'FAIL' on fail.
  */
  public function postComment($data) {
    $commentId = uniqid();

    $sql = "INSERT INTO Comment 
    (id, userId, videoId, comment) 
    VALUES (?, ?, ?, ?)";
    $sth = $this->db->prepare($sql);
    $sqlData = array($commentId, $data['username'],
      $data['videoId'], $data['commentText']);

    $sth->execute($sqlData);

    if ($sth->rowCount()==1) {
      $tmp['status'] = 'OK';
      $tmp['id'] = $commentId;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke lagt ut kommentaren!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Adds a rating of a video to the UserLike table
   * @param array of userId, videoId, and vote (1-5).
   * @return array with status 'OK' on success and 'FAIL' on fail.
  */
  public function rateVideo($data) {
    $sql = "INSERT INTO UserLike 
      (userId, videoId, vote) 
      VALUES ( :uId, :vId, :vote) 
      ON DUPLICATE KEY UPDATE 
        userId = :uId,
        videoId = :vId,
        vote = :vote";
  
    $sth = $this->db->prepare($sql);
    $sth->bindParam(':uId', $data['username'], PDO::PARAM_STR);
    $sth->bindParam(':vId', $data['videoId'], PDO::PARAM_STR);
    $sth->bindParam(':vote', $data['rate'], PDO::PARAM_INT);
    $sth->execute();

    if ($sth->rowCount()>=1) {
      $tmp['status'] = 'OK';
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['errorMessage'] = 'Fikk ikke lagt ut vurderingen!';
    }
    if ($this->db->errorInfo()[1]!=0) { // Error in SQL??????
      $tmp['errorMessage'] = $this->db->errorInfo()[2];
    }
    return $tmp;
  }

  /**
   * Searches through the video table, with a searchword.
   * We are using FULLTEXT INDEX on the name, userId and 
   * description collumns.
   * @param string word being searched for
   * @return array with status 'OK' on success and 'FAIL' on fail. 
   * With the data package filled with videos on a 'OK' and empty on 'FAIL'
  */
  public function search($search) {
    $sql = "SELECT * FROM Video
      WHERE MATCH (name, userId, description) 
      AGAINST (? IN BOOLEAN MODE)";
    $sth = $this->db->prepare($sql);
    $sqlData = array($search);
    $sth->execute($sqlData);

    $row = $sth->fetchAll();
    $tmp = [];
    if ($row) {
      foreach($row as $r) {
        $r['key'] = 'data:image/png;base64,'.base64_encode($r['thumbnail']); 
      }
      $tmp['status'] = 'OK';
      $tmp['data'] = $row;
    } else {
      $tmp['status'] = 'FAIL';
      $tmp['data'] = array();
    }
    return $tmp;
  }

  /* Taken from lecture about file uploading */
  public function scale ($img, $new_width, $new_height) {
    $old_x = imageSX($img);
    $old_y = imageSY($img);
  
    if($old_x > $old_y) {                     // Image is landscape mode
      $thumb_w = $new_width;
      $thumb_h = $old_y*($new_height/$old_x);
    } else if($old_x < $old_y) {              // Image is portrait mode
      $thumb_w = $old_x*($new_width/$old_y);
      $thumb_h = $new_height;
    } if($old_x == $old_y) {                  // Image is square
      $thumb_w = $new_width;
      $thumb_h = $new_height;
    }
  
    if ($thumb_w>$old_x) {                    // Don't scale images up
      $thumb_w = $old_x;
      $thumb_h = $old_y;
    }
  
    $dst_img = ImageCreateTrueColor($thumb_w,$thumb_h);
    imagecopyresampled($dst_img,$img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);
  
    ob_start();                         // flush/start buffer
    imagepng($dst_img,NULL,9);          // Write image to buffer
    $scaledImage = ob_get_contents();   // Get contents of buffer
    ob_end_clean();                     // Clear buffer
    return $scaledImage;
  }
}
?>